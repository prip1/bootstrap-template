from flask import Flask
from flask import render_template
import sys
import json

app = Flask(__name__)

@app.route("/")
def hello_world():
    data = {'username': 'Pang', 'site': 'stackoverflow.com'}
    sys.stdout = open('declare.js', 'w')
    jsonobj = json.dumps(data)
    print("var jsonstr = '{}' ".format(jsonobj))
    sys.stdout.flush()

    return render_template('index.html', data=data)

@app.route("/login")
def login():
    
    return render_template('login.html')